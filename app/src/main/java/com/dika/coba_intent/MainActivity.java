package com.dika.coba_intent;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnTugas2, btnTugas3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnTugas2 = (Button)findViewById(R.id.btn_tugas2);
        btnTugas3 = (Button)findViewById(R.id.btn_tugas3);

        btnTugas2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Tugas2Activity.class);
                startActivity(intent);
            }
        });

        btnTugas3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Tugas3Activity.class);
                startActivity(intent);
            }
        });
    }
}
